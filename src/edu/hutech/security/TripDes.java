/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.hutech.security;

import java.nio.ByteBuffer;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import sun.misc.BASE64Decoder;

/**
 *
 * @author khle
 */
public class TripDes {

    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_CHEME = "DESede";
    private KeySpec myKeySpec;
    private SecretKeyFactory mySecretKeyFactory;
    private Cipher cipher;
    byte[] keyAsBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
    public TripDes(){
        
    }

    public String encrypt(String plainText, String key) {
        byte[] encryptedText = null;
        try {
            cipher = Cipher.getInstance(DESEDE_ENCRYPTION_CHEME);
            cipher.init(Cipher.ENCRYPT_MODE, createSecretKey(key));
            byte[] bytes = plainText.getBytes(UNICODE_FORMAT);
            encryptedText = cipher.doFinal(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(Base64.getEncoder().encode(encryptedText));
    }

    public String decrypt(String encryptedString, String key) {
        String decryptedText = null;
        try {
            cipher = Cipher.getInstance(DESEDE_ENCRYPTION_CHEME);
            cipher.init(Cipher.DECRYPT_MODE, createSecretKey(key));
            BASE64Decoder base64decoder = new BASE64Decoder();
            byte[] encryptedText = base64decoder.decodeBuffer(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText = new String(plainText);
            System.out.println("chuoi plaintex :" + decryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }

    public SecretKey createSecretKey(String encryptionKey) throws Exception {
        byte[] keyMaterial = encryptionKey.getBytes(UNICODE_FORMAT); // Assume this one is 16 bytes. 

        byte[] newKeyBytes = ByteBuffer.wrap(new byte[24])
                .put(Arrays.copyOfRange(keyMaterial, 0, 16))
                .put(Arrays.copyOfRange(keyMaterial, 0, 8))
                .array();

        SecretKeyFactory mySecretKeyFactory = SecretKeyFactory.getInstance("DESede");
        KeySpec myKeySpec = new DESedeKeySpec(newKeyBytes);
        return mySecretKeyFactory.generateSecret(myKeySpec);
    }
}
